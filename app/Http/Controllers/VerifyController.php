<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class VerifyController extends Controller
{
    public function getVerify(){
        return view('verify');
    }

    public function postVerify(Request $request){
        if ($user=User::where('code', $request->code)->first()) {
            $user->active=1;
            $user->code=null;
            $user->save();
            return redirect()->route('home')->withMessage('Tu cuenta está activa');

        }
        else{
            return back()->withMessage('Verifica tu código'); 
        }
    }
}
