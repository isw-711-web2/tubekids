<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $now = Carbon::now();
        
        if($request->wantsJson()){
        return Video::where('user_id', auth()->id())->get();
        }

        //  $videos = Video::all()->where('user_id', auth()->id)->get();

        //  return response()->json($videos);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $this->validate($request, [
            'name' => 'required',
            'url' => 'required', 'url'
        ]);
        
        // return Playlist::create([
        //    'name' => $request['name'],
        //    'url' => $request['url'],
        //   'user_id' ->user_id = auth()->id();
        // ]);

        $video = new Video();
        $video->name = $request->name;
        $video->url = $request->url;
        $video->user_id = auth()->id();
        $video->save();
    
        return $video;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        // $this->validate($request, [
        //     'name' => 'name',
        //     'url' => 'required',
        //  ]);

        //  $perfil = Perfil::findOrFail($id);

        //  $perfil->update($request->all());

        $video = Video::find($id);
        $video->name = $request->name;
        $video->url = $request->url;
        $video->save();
        
        return response()->json($video, 201);
        // return $video;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $video = Video::findOrFail($id);
        $video->delete();
        return response()->json([
         'message' => 'Video deleted successfully', 'status', 200
        ]);
    }
}
