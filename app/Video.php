<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Video extends Model
{
    use Notifiable;
    
    // protected $table = 'videos';

    protected $fillable = [
        'name', 'url',
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
