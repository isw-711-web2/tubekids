<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('mail', function () {
//     $user = App\User::find(1);

//     return new App\Mail\ActivateUserAccount($user);
// });

Route::get('/', 'GuestController@welcome');
Route::get('/activate-account/{token}', 'GuestController@verify')->name('activate-account');


Route::resource('/profiles', 'ProfileController')->middleware('auth');

Auth::routes();
Route::resource('video', 'VideoController')->middleware('auth');
Route::resource('playlist', 'PlaylistController')->middleware('auth');
Route::get('/home', 'HomeController@index')->name('home');



// Auth::routes(['verify' => true]);

// Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

// Route::get('/verify', 'VerifyController@getVerify')->name('getverify');
// Route::post('/verify', 'VerifyController@postVerify')->name('verify');

// Auth::routes();



