import Video from './admin/VideoComponent.vue'
import Playlist from './admin/PlaylistComponent.vue'
export const routes = [
    { 
        path:'/videos',
        component:Video
    },
    { 
        path:'/playlists',
        component:Playlist
    },
 
];