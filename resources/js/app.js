/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue');




//Import progressbar
require('./progressbar'); 

//Setup custom events 
require('./customEvents'); 

// window.Vue = require('vue');
import VueRouter from  'vue-router';
import VueYoutube from 'vue-youtube';
import Notifications from 'vue-notification';
import Embed from 'v-video-embed'


Vue.use(VueRouter);
Vue.use(VueYoutube);
Vue.use(Notifications);
Vue.use(Embed);
// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
// Vue.component('youtube-dashboard', require('./Youtube/YoutubeDash.vue').default);
import Swal from 'sweetalert2'
window.Swal = Swal
const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  onOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer)
    toast.addEventListener('mouseleave', Swal.resumeTimer)
  }
})
window.Toast = Toast

//Import v-from
import { Form, HasError, AlertError } from 'vform'
window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)



//Global
window.eventBus = new Vue({});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
//Rutas
import { routes } from './routes';

 const router = new VueRouter({
     routes,
     mode: 'hash',
 });

const app = new Vue({
    router
}).$mount('#app');
